import sirv from "sirv";
import polka from "polka";
import compression from "compression";
import * as sapper from "@sapper/server";

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === "development";

polka() // You can also use Express
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware({
      session: (_req: any, _res: any) => ({
        url: process.env.SUPABASE_URL,
        key: process.env.SUPABASE_KEY,
        openViduUrl: process.env.OPENVIDU_URL,
        openViduSecret: process.env.OPENVIDU_SECRET,
      }),
    })
  )
  .listen(PORT, (err) => {
    if (err) console.log("error", err);
  });
